# Build the main image
FROM php:7.4.2-apache-buster

RUN pecl install -f xdebug \
    && echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" > /usr/local/etc/php/conf.d/xdebug.ini;

COPY ./xdebug.ini /usr/local/etc/php/conf.d/

# Note: The following is inherited from the base image, no need to execute it:
# EXPOSE 80
# CMD ["apache2-foreground"]
